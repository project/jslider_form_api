/**
 * This script adds jQuery slider functionality to transform_slider form element
 */
Drupal.behaviors.jsliderFormAPIBehavior = function (context) {

  // Create sliders
  $("div.transfer-slider-container:not(.transferSlider-processed)", context).each( function () {
    $(this).addClass("transferSlider-processed");
    
    // Get values
    var Container = $(this).parents("div.container-inline");
    var left  = Container.find("div.transfer-slider-left-field").find("input").val() - 0;
    var right = Container.find("div.transfer-slider-right-field").find("input").val() - 0;
    
    // Setup slider
    $(this).slider({
      max: (left + right),
      startValue: left,
      range: false,
      slide: jsliderSlideProcess,
      change: jsliderSlideProcess
    });
    
  });
  
  // Bind left textfield changes
  $("div.transfer-slider-left-field:not(.transferSlider-processed)", context)
    .addClass("transferSlider-processed")
    .find("input").keyup( function (e) {
    
      $(this).addClass("transferSlider-processed");
      
      // Get container
      var Container = $(this).parents("div.container-inline");
      
      // Left input value
      var left = $(this).val() - 0;
      if (isNaN(left)) {
        left = 0;
        Container.find("div.transfer-slider-left-field").find("input").val(left);
      }
      
      // Get slider
      var jSlider = Container.find("div.transfer-slider-container");
      
      // Get max
      var max = jSlider.slider("option", "max");
      max = max['x'];
      
      // Validate left input
      if (left > max) {
        left = max;
        // Setup left value
        Container.find("div.transfer-slider-left-field").find("input").val(left);
      }
      
      // Setup right value
      Container.find("div.transfer-slider-right-field").find("input").val(max - left);
      
      // Move slider without toggling events
      jSlider.slider("moveTo", left, null, true);
  });
  
  // Bind right textfield changes
  $("div.transfer-slider-right-field:not(.transferSlider-processed)", context)
    .addClass("transferSlider-processed")
    .find("input").keyup( function (e) {
    
      // Get container
      var Container = $(this).parents("div.container-inline");
      
      // Right input value
      var right = $(this).val() - 0;
      if (isNaN(right)) {
        right = 0;
        Container.find("div.transfer-slider-right-field").find("input").val(right);
      }
      
      // Get slider
      var jSlider = Container.find("div.transfer-slider-container");
      
      //Get max
      var max = jSlider.slider("option", "max");
      max = max['x'];
      
      // Validate right
      if (right > max) {
        right = max;
        // Setup right value
        Container.find("div.transfer-slider-right-field").find("input").val(right);
      }
      
      // Setup left value
      Container.find("div.transfer-slider-left-field").find("input").val(max - right);
      
      // Move slider without toggling events
      jSlider.slider("moveTo", max - right, null, true);
  });
}

// Slider processor
var jsliderSlideProcess = function(event, ui) {
  // Setup values
  var Container = $(this).parents("div.container-inline");
  Container.find("div.transfer-slider-left-field").find("input").val(ui.value);
  var max = $(this).slider("option", "max");
  max = max['x'];
  Container.find("div.transfer-slider-right-field").find("input").val(max - ui.value);
}
